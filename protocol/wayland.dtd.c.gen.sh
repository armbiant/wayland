#!/bin/bash
# Copyright 2019 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

set -eo pipefail

# This script is used to regenerate the wayland.dtd.h file. See ../README.fuchsia for details.

cols=16
sym="DTD_DATA"
file=${1}
size=$(stat -c %s ${file})
full=$((((${size} / ${cols})) * ${cols}))

header="// Copyright $(date +%Y) The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file."$'\n\n'
header="${header}#pragma pack(4)"$'\n\n'
header="${header}char ${sym}_begin[] = {"$'\n'
header="${header}$(hexdump -n ${full} -v -e '" "' -e "${cols}/1 \" 0x%02X,\"" -e '"\n"' ${file})"$'\n'
header="${header} $(hexdump -s ${full} -v -e '/1 ""' -e '/1 " 0x%02X,"' ${file})"$'\n'
header="${header}};"$'\n\n'
header="${header}int ${sym}_len = ${size};"$'\n'

printf "${header}"
